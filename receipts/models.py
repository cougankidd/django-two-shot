from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


# model with a name property that contains characters with a
#  maximum length of 50 characters
# an owner property that is a foreign key to the User model with:
# a related name of "categories"
# a cascade deletion relation
class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        User,
        related_name="categories",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


# a name property that contains characters with a maximum length of 100 characters
# a number property that contains characters (not numbers) with a maximum length of 20 characters
# an owner property that is a foreign key to the User model with:
# a related name of "accounts"
# a cascade deletion relation
class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        User,
        related_name="accounts",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


# a vendor property that contains characters with a maximum length of 200 characters
# a total property that is a DecimalField with:
# three decimal places
# a maximum of 10 digits
# a tax property that is a DecimalField with:
# three decimal places
# a maximum of 10 digits
# a date property that contains a date and time of when the transaction took place
# a purchaser property that is a foreign key to the User model with:
# a related name of "receipts"
# a cascade deletion relation
# a category property that is a foreign key to the ExpenseCategory model with:
# a related name of "receipts"
# a cascade deletion relation
# an account property that is a foreign key to the Account model with:
# a related name of "receipts"
# a cascade deletion relation
# allowed to be null
class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField(default=timezone.now)
    purchaser = models.ForeignKey(
        User,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        Account,
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )
